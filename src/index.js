const express = require('express')
const cors = require('cors')
const db = require('./database')

class ControllerApi {
    constructor()
    {
        this.express = express();
        this.middlewares();
        this.routes();
    }

    middlewares(){
        this.express.use(express.json())
        this.express.use(express(cors()))
    }

    routes()
    {
        const rotas = require('./routes/routes')
        this.express.use('/hotel/', rotas)
        this.express.get('/testeHotel/', (req, res) => {
            db.query('SELECT * FROM CLIENTES_HOTEL', (erro, resultado) => {
                if (erro) {
                    res.status(200).json({ error: 'Não foi possível conectar a base dos dados SQL.', erro });
                } else {
                    res.status(200).json(resultado);
                }
            });
        });
    }
}

module.exports = new ControllerApi().express