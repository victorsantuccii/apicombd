const db = require('../database');

class ReservaController {
  constructor() {}

  static criarReserva(req, res) {
    const { id_quarto, id_usuario, data_entrada, data_saida } = req.body;

    if (id_quarto === undefined || id_usuario === undefined || data_entrada === undefined || data_saida === undefined) {
      return res.status(400).json({ error: 'Preencha os campos corretamente.' });
    }

    db.query(
      'INSERT INTO reservas (id_quarto, id_usuario, data_entrada, data_saida, valor_total) VALUES (?, ?, ?, ?, 0.00)',
      [id_quarto, id_usuario, data_entrada, data_saida],
      (error, results) => {
        if (error) {
          return res.status(500).json({ error: 'Erro ao criar reserva: ' + error.message });
        }

        
        const reservaId = results.insertId;
        db.query(
          'UPDATE reservas r JOIN quartos q ON r.id_quarto = q.id SET r.valor_total = DATEDIFF(r.data_saida, r.data_entrada) * q.valor_diaria WHERE r.id = ?',
          [reservaId],
          (updateError) => {
            if (updateError) {
              return res.status(500).json({ error: 'Erro ao atualizar valor da reserva: ' + updateError.message });
            }
            res.status(201).json({ message: 'Reserva criada com sucesso!' });
          }
        );
      }
    );
  }

  static buscarTodasReservas(req, res) {
    db.query('SELECT * FROM reservas', (error, results) => {
      if (error) {
        return res.status(500).json({ error: 'Erro ao buscar reservas: ' + error.message });
      }
      res.status(200).json(results);
    });
  }

  static buscarReservaPorId(req, res) {
    const id = req.params.id;

    db.query('SELECT * FROM reservas WHERE id = ?', [id], (error, results) => {
      if (error) {
        return res.status(500).json({ error: 'Erro ao buscar reserva: ' + error.message });
      }
      if (results.length === 0) {
        return res.status(404).json({ error: 'Reserva não encontrada.' });
      }
      res.status(200).json(results[0]);
    });
  }

  static atualizarReserva(req, res) {
    const id = req.params.id;
    const { id_quarto, id_usuario, data_entrada, data_saida, valor_total } = req.body;

    db.query(
      'UPDATE reservas SET id_quarto = ?, id_usuario = ?, data_entrada = ?, data_saida = ?, valor_total = ? WHERE id = ?',
      [id_quarto, id_usuario, data_entrada, data_saida, valor_total, id],
      (error, results) => {
        if (error) {
          return res.status(500).json({ error: 'Erro ao atualizar reserva: ' + error.message });
        }
        res.status(200).json({ message: 'Reserva atualizada com sucesso!' });
      }
    );
  }

  static excluirReserva(req, res) {
    const id = req.params.id;

    db.query('DELETE FROM reservas WHERE id = ?', [id], (error, results) => {
      if (error) {
        return res.status(500).json({ error: 'Erro ao excluir reserva: ' + error.message });
      }
      res.status(200).json({ message: 'Reserva excluída com sucesso!' });
    });
  }

  static detalhesReservas(req, res) {
    const query = `
      SELECT 
        r.id AS id_reserva,
        u.nome AS nome_usuario,
        r.data_entrada,
        r.data_saida,
        r.valor_total,
        q.numero_quarto,
        q.valor_diaria,
        DATEDIFF(r.data_saida, r.data_entrada) AS total_dias,
        DATEDIFF(r.data_saida, r.data_entrada) * q.valor_diaria AS total_valor_diarias
      FROM 
        reservas r
      JOIN 
        quartos q ON r.id_quarto = q.id
      JOIN
        usuario u ON r.id_usuario = u.id;
    `;

    db.query(query, (error, results) => {
      if (error) {
        return res.status(500).json({ error: 'Erro ao buscar detalhes das reservas: ' + error.message });
      }
      res.status(200).json(results);
    });
  }
}

module.exports = ReservaController;