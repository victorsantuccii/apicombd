const db = require('../database');

class QuartoController {
  constructor() {}

  static criarQuarto(req, res) {
    const { numero, andar, numero_quarto, tipo_quarto, valor_diaria, status } = req.body;

    if (numero === undefined || andar === undefined || numero_quarto === undefined || tipo_quarto === "" || valor_diaria === undefined) {
      return res.status(400).json({ error: 'Preencha os campos corretamente.' });
    }

    db.query(
      'INSERT INTO quartos (numero, andar, numero_quarto, tipo_quarto, valor_diaria, status) VALUES (?, ?, ?, ?, ?, ?)',
      [numero, andar, numero_quarto, tipo_quarto, valor_diaria, status || 'Disponível'],
      (error, results) => {
        if (error) {
          return res.status(500).json({ error: 'Erro ao criar quarto: ' + error.message });
        }
        res.status(201).json({ message: 'Quarto criado com sucesso!' });
      }
    );
  }

  static buscarTodosQuartos(req, res) {
    db.query('SELECT * FROM quartos', (error, results) => {
      if (error) {
        return res.status(500).json({ error: 'Erro ao buscar quartos: ' + error.message });
      }
      res.status(200).json(results);
    });
  }

  static buscarQuartoPorId(req, res) {
    const id = req.params.id;

    db.query('SELECT * FROM quartos WHERE id = ?', [id], (error, results) => {
      if (error) {
        return res.status(500).json({ error: 'Erro ao buscar quarto: ' + error.message });
      }
      if (results.length === 0) {
        return res.status(404).json({ error: 'Quarto não encontrado.' });
      }
      res.status(200).json(results[0]);
    });
  }

  static atualizarQuarto(req, res) {
    const id = req.params.id;
    const { numero, andar, numero_quarto, tipo_quarto, valor_diaria, status } = req.body;

    db.query(
      'UPDATE quartos SET numero = ?, andar = ?, numero_quarto = ?, tipo_quarto = ?, valor_diaria = ?, status = ? WHERE id = ?',
      [numero, andar, numero_quarto, tipo_quarto, valor_diaria, status, id],
      (error, results) => {
        if (error) {
          return res.status(500).json({ error: 'Erro ao atualizar quarto: ' + error.message });
        }
        res.status(200).json({ message: 'Quarto atualizado com sucesso!' });
      }
    );
  }

  static excluirQuarto(req, res) {
    const id = req.params.id;

    db.query('DELETE FROM quartos WHERE id = ?', [id], (error, results) => {
      if (error) {
        return res.status(500).json({ error: 'Erro ao excluir quarto: ' + error.message });
      }
      res.status(200).json({ message: 'Quarto excluído com sucesso!' });
    });
  }
}

module.exports = QuartoController;
