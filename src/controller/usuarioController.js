const db = require('../database');

class UsuarioController {
  constructor() {}

  static criarUsuario(req, res) {
    const { nome, email, cpf, senha, telefone, cep } = req.body;

    if (!nome || !email || !cpf || !senha || !telefone || !cep) {
      return res.status(400).json({ error: 'Preencha todos os campos corretamente.' });
    }

    db.query(
      'INSERT INTO usuario (nome, email, cpf, senha, telefone, cep) VALUES (?, ?, ?, ?, ?, ?)',
      [nome, email, cpf, senha, telefone, cep],
      (error, results) => {
        if (error) {
          return res.status(500).json({ error: 'Erro ao criar usuário: ' + error.message });
        }
        res.status(201).json({ message: 'Usuário criado com sucesso!' });
      }
    );
  }

  static buscarTodosUsuarios(req, res) {
    db.query('SELECT * FROM usuario', (error, results) => {
      if (error) {
        return res.status(500).json({ error: 'Erro ao buscar usuários: ' + error.message });
      }
      res.status(200).json(results);
    });
  }

  static buscarUsuarioPorId(req, res) {
    const id = req.params.id;

    db.query('SELECT * FROM usuario WHERE id = ?', [id], (error, results) => {
      if (error) {
        return res.status(500).json({ error: 'Erro ao buscar usuário: ' + error.message });
      }
      if (results.length === 0) {
        return res.status(404).json({ error: 'Usuário não encontrado.' });
      }
      res.status(200).json(results[0]);
    });
  }

  static atualizarUsuario(req, res) {
    const id = req.params.id;
    const { nome, email, cpf, senha, telefone, cep } = req.body;

    db.query(
      'UPDATE usuario SET nome = ?, email = ?, cpf = ?, senha = ?, telefone = ?, cep = ? WHERE id = ?',
      [nome, email, cpf, senha, telefone, cep, id],
      (error, results) => {
        if (error) {
          return res.status(500).json({ error: 'Erro ao atualizar usuário: ' + error.message });
        }
        res.status(200).json({ message: 'Usuário atualizado com sucesso!' });
      }
    );
  }

  static excluirUsuario(req, res) {
    const id = req.params.id;

    db.query('DELETE FROM usuario WHERE id = ?', [id], (error, results) => {
      if (error) {
        return res.status(500).json({ error: 'Erro ao excluir usuário: ' + error.message });
      }
      res.status(200).json({ message: 'Usuário excluído com sucesso!' });
    });
  }
}

module.exports = UsuarioController;
